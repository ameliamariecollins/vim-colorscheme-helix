
highlight clear
set background=dark

if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "helix"

"Automatically reload the colorscheme when saving this file
if has('autocmd')
  augroup HelixColorScheme
    autocmd!
    autocmd BufWritePost */colors/helix.vim call oxime#colorscheme_refresh()
  augroup END
endif

"-- Python-specific
"hi pythonBuiltin ctermfg=LightCyan
hi link pythonComment Comment
"hi pythonConditional
"hi pythonDecorator
"hi pythonDecoratorName
hi link pythonDocstring Comment
"hi pythonEscape
"hi pythonException
"hi pythonFunction
hi link pythonNumber Number
"hi pythonOperator
"hi pythonPreCondit
"hi pythonRawString
"hi pythonRepeat
"hi PythonSpaceError ctermbg=52 guibg=darkred
hi link pythonStatement Statement
hi link pythonString String
"hi pythonSync

"hi Normal guifg=#bbbbcc guibg=#070710
hi Normal gui=NONE guifg=#a0a0a0 guibg=#050508
hi Normal gui=NONE guifg=#a0a097 guibg=#050508
"hi Normal gui=NONE guifg=red guibg=#050508
hi Cursor guibg=#d00000

hi CursorLine	gui=NONE 		guibg=#101018
hi CursorLineNr	gui=bold guifg=#c0c0c0	guibg=#4e4e4e
hi CursorColumn	gui=NONE		guibg=#101018
hi LineNr	gui=NONE guifg=#707090  guibg=#181820
hi Visual    cterm=reverse gui=reverse guibg=#333355
hi VisualNOS	guibg=#5f00ff

hi Comment	gui=NONE guifg=#606060
hi Character	gui=NONE guifg=#00b000
hi Constant	gui=NONE guifg=#ffffd7
hi Error	gui=NONE guifg=#dfaf00	guibg=#870000
hi Identifier	gui=NONE guifg=#008787
"hi Ignore      
"hi IndentGuidesEven
"hi IndentGuidesOdd
"hi lCursor
hi Label	gui=NONE guifg=#00b7b7
hi MatchParen	gui=NONE guifg=#ffff00	guibg=#262626
hi Number	gui=NONE guifg=#5faf5f
"hi PreProc			ctermfg=125
"hi PreProc			ctermfg=167
"via Todd:
"hi PreProc			ctermfg=174
"hi PreProc			ctermfg=175
"hi PreProc			ctermfg=168
"hi PreProc			ctermfg=170
hi PreProc	gui=NONE guifg=#af5f87
hi Special	gui=NONE guifg=#87005f
hi Statement	gui=NONE guifg=#5f5faf
hi String	gui=NONE guifg=#00875f
hi Todo		gui=NONE guifg=#0000ff guibg=#ffaf00
hi Type		gui=NONE guifg=#0087ff
"via Todd
"hi Type				ctermfg=111
"hi Type				ctermfg=75
hi Type		gui=NONE guifg=#5f87ff
"hi Underlined  

hi Folded	gui=NONE guifg=#af5fff guibg=#5f005f

"hi CursorLine					guibg=#101018
"hi CursorColumn					guibg=#101018
hi SignColumn guifg=#dddddd guibg=#333355
hi DiffAdd guifg=#33cccc guibg=#005555
hi DiffDelete guifg=#993344 guibg=#660033
hi DiffChange guifg=#eeaaff guibg=#886699
"hi IncSearch	gui=reverse
"hi ModeMsg	gui=bold
"hi VertSplit	gui=reverse
"hi DiffText	gui=bold 			guibg=Red
"hi Cursor					guibg=red
"hi lCursor	guibg=Cyan
"hi MoreMsg	gui=bold 	guifg=SeaGreen
"hi Question	gui=bold 	guifg=SeaGreen
"hi Search			guifg=NONE	guibg=Yellow
"hi SpecialKey			guifg=#302060
hi SpecialKey			guifg=#5f40ff
hi Title	gui=bold	guifg=White
hi WarningMsg			guifg=#ff8700
hi WildMenu			guifg=#ffffd7	guibg=#500000 
hi StatusLine   gui=reverse     guifg=#505050	guibg=#c0c0c0
hi StatusLineNC   gui=reverse   guifg=#202020	guibg=#909090
"hi TabLineFill gui=NONE			guifg=#ff0000	guibg=#303030
"hi TabLineSel gui=NONE			guifg=#ff0000	guibg=#303030
"hi lCursor gui=NONE			guifg=#ff0000	guibg=#303030
"hi Folded			guifg=DarkBlue	guibg=LightGrey
"hi FoldColumn			guifg=DarkBlue	guibg=Grey
"hi DiffAdd					guibg=LightBlue
"hi DiffChange					guibg=LightMagenta
"hi DiffDelete	gui=bold	guifg=Blue	guibg=LightCyan
"hi StatusLine			guifg=#404070	guibg=#b0b0d0
"hi StatusLineNC		guifg=#000050	guibg=gold
hi NonText	gui=bold	guifg=#901020
hi ColorColumn		gui=bold		guibg=#300010
hi OverLength					guibg=#300010
hi PythonOverLength					guibg=#300010
hi PythonDot	gui=None				guifg=#b0b0ee
hi TrailingSpace	gui=NONE			guifg=#592929    
"hi Select					guibg=yellow
"hi LineNr

"-- Syntax
"hi PreProc			guifg=#bb2299
"hi Identifier			guifg=darkcyan
"hi Comment			guifg=#666666
"hi Constant			guifg=cyan
"hi Special			guifg=#aa00bb
hi SpecialComment cterm=italic	gui=italic		guifg=#225577

"hi Statement	gui=bold	guifg=#2222dd
"hi Type	gui=bold	guifg=#4444aa
"hi String			guifg=grey85
"hi Character			guifg=violet
"hi Todo
"hi MatchParen	gui=bold	guifg=cyan		guibg=darkcyan

" completion colors
"hi Pmenu	cterm=NONE	ctermfg=LightGrey	ctermbg=237
"hi PmenuSel	cterm=NONE	ctermfg=White		ctermbg=242

hi Pmenu guibg=#262626
hi PmenuSel guifg=White guibg=#4e4e4e
"hi PmenuSbar guibg=LightGrey guibg=DarkGrey
"hi PmenuThumb gui=reverse

"hi WildMenu guifg=White guibg=firebrick

hi VimwikiItalic gui=italic
hi VimwikiBold gui=bold 
hi VimwikiBoldItalic gui=bold,italic

"hi link SyntasticStyleWarning SpellBad
"hi link SyntasticWarning SpellBad

"hi SyntasticWarning gui=undercurl guisp=#ffc000
"hi link SyntasticStyleWarning SyntasticWarning

hi ALEWarning gui=bold guifg=#000050 guibg=#feB73b
hi ALEError gui=bold guifg=#ffff00 guibg=#dd0000
hi ALEVirtualTextError guifg=#eeeeee guibg=#990000
hi ALEVirtualTextWarning guifg=#eeeeee guibg=#bbaa00
hi ALEVirtualTextWarning guifg=#333366 guibg=#feb73b
hi ALEVirtualTextInfo guifg=#eeeeee guibg=#00aa77

if has('gui_running')
  finish
endif

"=== MONOCHROME ==============================================================
"hi ColorColumn		term=reverse
"hi DiffAdd		term=bold
"hi DiffChange		term=bold
"hi DiffDelete		term=bold
"hi DiffText		term=reverse
"hi Directory		term=bold
"hi ErrorMsg		term=standout
"hi FoldColumn		term=standout
"hi Folded		term=standout
"hi IncSearch		term=reverse
"hi LineNr		term=reverse
"hi ModeMsg		term=bold
"hi MoreMsg		term=bold
"hi NonText		term=bold
"hi Question		term=standout
"hi Search		term=reverse
"hi Select		term=reverse
"hi SpecialKey		term=bold
"hi StatusLineNC	term=reverse
"hi StatusLine		term=reverse
"hi Title		term=bold
"hi VertSplit		term=reverse
"hi VisualNOS		term=reverse
hi Visual		term=reverse
"hi WarningMsg		term=standout
"hi WildMenu		term=standout
hi CursorColumn		term=reverse
hi CursorLine		term=reverse
hi CursorLineNr		term=bold

"-- Syntax
"hi Comment		term=NONE
"hi Constant		term=underline
"hi Identifier		term=underline
hi MatchParen		term=reverse
"hi Number		term=bold
"hi PreProc		term=underline
"hi Special		term=bold
"hi Statement		term=bold
"hi Todo		term=bold
"hi Type		term=underline

if &t_Co < 8
  finish
endif

"=== BASELINE ================================================================
"-- Normal text (add ctermbg=black for opacity on transparent/light terms)
"hi Normal ctermbg=black

"=== 8 COLORS ================================================================
if &t_Co == 8
  hi CursorColumn	cterm=reverse
  hi CursorLine	cterm=reverse
  hi CursorLineNr cterm=NONE	ctermfg=White		ctermbg=Blue
  hi Visual cterm=bold  ctermbg=blue ctermfg=White
" hi Visual		term=reverse

  hi Character			ctermfg=DarkGreen
  hi Comment			ctermfg=DarkGray
  hi Constant			ctermfg=White
  "hi Error       
  hi Identifier			ctermfg=DarkCyan
  "hi Ignore      
  "hi IndentGuidesEven
  "hi IndentGuidesOdd
  "hi lCursor
  hi Label	ctermfg=LightCyan
  hi MatchParen			ctermfg=yellow
  hi Number			ctermfg=LightGreen
  hi PreProc			ctermfg=DarkMagenta
  hi Special	cterm=bold	ctermfg=DarkMagenta
  hi Statement	cterm=NONE	ctermfg=DarkBlue
  hi String			ctermfg=DarkGreen
  hi Todo		cterm=bold	ctermfg=yellow		ctermbg=red 
  hi Type				ctermfg=Darkblue

  finish
endif

"=== 16 COLORS ===============================================================
hi Comment			ctermfg=DarkGrey
hi Character			ctermfg=DarkGreen
hi Comment			ctermfg=DarkGrey
hi Constant			ctermfg=White
"hi Error
hi Identifier			ctermfg=DarkCyan
"hi Ignore     
"hi IndentGuidesEven
"hi IndentGuidesOdd
  hi Label	ctermfg=LightCyan
"hi lCursor
hi MatchParen			ctermfg=yellow
hi Number			ctermfg=LightGreen
hi PreProc			ctermfg=DarkMagenta
hi Special	cterm=bold	ctermfg=DarkMagenta
hi Statement	cterm=NONE	ctermfg=DarkBlue
hi String			ctermfg=DarkGreen
hi Todo		cterm=bold	ctermfg=yellow		ctermbg=red 
hi Type				ctermfg=Darkblue
"hi Underlined  

hi ColorColumn	cterm=NONE			ctermbg=DarkRed
"hi Conceal
hi CursorColumn	cterm=NONE			ctermbg=DarkGrey
hi CursorLine		cterm=NONE			ctermbg=DarkGrey
hi CursorLineNr	cterm=NONE	ctermfg=Black	ctermbg=Grey
"hi DiffAdd						ctermbg=LightBlue
"hi DiffChange						ctermbg=LightMagenta
"hi DiffDelete			ctermfg=Blue		ctermbg=LightCyan
"hi DiffText	cterm=bold 				ctermbg=Red
"hi Directory			ctermfg=DarkBlue
"hi ErrorMsg			ctermfg=Yellow		ctermbg=DarkRed
"hi FoldColumn
"hi Folded			ctermfg=DarkBlue	ctermbg=Grey
"hi IncSearch	cterm=reverse
hi LineNr	cterm=italic		ctermfg=Grey		ctermbg=darkgray
"hi ModeMsg	cterm=bold
"hi MoreMsg			ctermfg=DarkGreen
hi NonText			ctermfg=Red
hi OverLength			ctermfg=white		ctermbg=Red
hi Pmenu			ctermfg=LightGrey	ctermbg=DarkGrey
hi PmenuSBar			ctermbg=Grey		ctermfg=White
hi PmenuSelect			ctermbg=White
"hi PmenuThumb
"hi Question			ctermfg=DarkGreen
"hi Search						ctermbg=Yellow
"hi Select			ctermbg=yellow		ctermbg=red
"hi SignColumn
hi SpecialKey cterm=NONE			ctermfg=DarkBlue
"hi SpellBad
"hi SpellCap
"hi SpellLocal
"hi SpellRare
"hi StatusLine			ctermfg=white 		ctermbg=darkgrey
"hi StatusLineNC			ctermfg=black		ctermbg=darkgrey
hi Title	cterm=bold		ctermfg=White
"hi TabLine
"hi TabLineFill
"hi TabLineSel
"hi TrailingSpace					ctermbg=magenta
"hi VertSplit	cterm=reverse
hi Visual					ctermbg=DarkGray
hi VisualNOS	cterm=NONE 				ctermbg=DarkMagenta
hi WarningMsg			ctermfg=Yellow
"hi WildMenu			ctermfg=Black		ctermbg=Yellow 

if &t_Co <= 16
  finish
endif

"=== 88 COLORS ===============================================================
"if &t_Co == 88
"  hi CursorColumn	cterm=NONE 	ctermbg=DarkGrey
"  hi CursorLine		cterm=NONE 	ctermbg=DarkGrey
"endif

"=== 256 COLORS ==============================================================
if &t_Co == 256
  hi Normal		ctermfg=7
  hi CursorColumn					ctermbg=233
  hi CursorLineNr	ctermfg=black			ctermbg=239
  hi CursorLine						ctermbg=233
  hi LineNr						ctermbg=233
  hi ColorColumn	cterm=NONE			ctermbg=52
  hi Visual cterm=reverse						ctermbg=DarkGrey
  hi VisualNOS		cterm=NONE 			ctermbg=57

  hi Comment			ctermfg=241
  hi Character			ctermfg=34
  hi Constant			ctermfg=230
  hi Error       		ctermfg=178 ctermbg=88
  hi Identifier	cterm=NONE		ctermfg=30
  "hi Ignore      
  "hi IndentGuidesEven
  "hi IndentGuidesOdd
  "hi lCursor
  hi MatchParen			ctermfg=226 ctermbg=235
  hi Number			ctermfg=71
  hi PreProc			ctermfg=125
  "hi PreProc			ctermfg=167
  "via Todd:
  hi PreProc			ctermfg=174
  hi PreProc			ctermfg=175
  hi PreProc			ctermfg=168
  hi PreProc			ctermfg=170
  hi PreProc			ctermfg=132
  hi Special	cterm=NONE	ctermfg=89
  hi Statement	cterm=NONE	ctermfg=61
  hi String			ctermfg=29
  hi Todo		cterm=bold	ctermfg=21		ctermbg=214 
  hi Type				ctermfg=33
  "via Todd
  "hi Type				ctermfg=111
  "hi Type				ctermfg=75
  hi Type				ctermfg=69
  hi Underlined  cterm=underline	ctermfg=33
  hi WarningMsg			ctermfg=208

  hi Folded			ctermfg=135	ctermbg=53

  "-- Python-specific
  "hi pythonBuiltin ctermfg=LightCyan
  hi link pythonComment Comment
  "hi pythonConditional
  "hi pythonDecorator
  "hi pythonDecoratorName
  hi link pythonDocstring Comment
  "hi pythonEscape
  "hi pythonException
  "hi pythonFunction
  "hi pythonBrackets guifg=yellow
  hi link pythonNumber Number
  "hi pythonOperator
  "hi pythonPreCondit
  "hi pythonRawString
  "hi pythonRepeat
  "hi PythonSpaceError ctermbg=52 guibg=darkred
  hi link pythonStatement Statement
  hi link pythonString String
  "hi pythonSync

  hi StatusLine   cterm=reverse     ctermfg=238	ctermbg=248
  hi StatusLineNC   cterm=reverse   ctermfg=233	ctermbg=239
  hi Pmenu	cterm=NONE	ctermfg=LightGrey	ctermbg=237
  hi PmenuSel	cterm=NONE	ctermfg=White		ctermbg=242
  "hi PmenuSBar			ctermfg=White		ctermbg=Grey
  "hi PmenuThumb

  hi VimwikiItalic ctermfg=254
  hi VimwikiBold cterm=bold 
  hi VimwikiBoldItalic cterm=bold ctermfg=white ctermbg=236 
endif



" vim:sw=2
